#include <conio.h>
#include <graphics.h>

#define High 480  // 游戏画面尺寸
#define Width 640

// 全局变量
int ball_x,ball_y; // 小球的坐标
int ball_vx,ball_vy; // 小球的速度
int radius;  // 小球的半径
int bar_x,bar_y;//挡板中心坐标
int bar_high,bar_width;//挡板长宽
int bar_left,bar_right,bar_top,bar_bottom;//挡板位置

void startup()  // 数据初始化
{
	ball_x = Width/2;
	ball_y = High/2;
	ball_vx = 1;
	ball_vy = 1;
	radius = 20;
	bar_x=320;
	bar_y=10;
	bar_high=20;
	bar_width=100;
	
	initgraph(Width, High);
	BeginBatchDraw();
}

void clean()  // 显示画面
{
	// 绘制黑线、黑色填充的圆
	setcolor(BLACK);
	setfillcolor(BLACK);
	fillcircle(ball_x, ball_y, radius);
}	

void show()  // 显示画面
{
	// 绘制黄线、绿色填充的圆
	setcolor(YELLOW);
	setfillcolor(GREEN);
	fillcircle(ball_x, ball_y, radius);	
	FlushBatchDraw();
	// 延时
	Sleep(3);
	//绘制挡板
	setcolor(BLUE);
	setfillcolor(BLUE);
	rectangle(220,480,420,440);
}	

void updateWithoutInput()  // 与用户输入无关的更新
{
	// 更新小圆坐标
	ball_x = ball_x + ball_vx;
	ball_y = ball_y + ball_vy;
	
	if ((ball_x<=radius)||(ball_x>=Width-radius))
		ball_vx = -ball_vx;
	if ((ball_y<=radius)||(ball_y>=High-radius))
		ball_vy = -ball_vy;	
}

void updateWithInput()  // 与用户输入有关的更新
{	
}

void gameover()
{
	EndBatchDraw();
	closegraph();
}

int main()
{
	startup();  // 数据初始化	
	while (1)  //  游戏循环执行
	{
		clean();  // 把之前绘制的内容清除
		updateWithoutInput();  // 与用户输入无关的更新
		updateWithInput();     // 与用户输入有关的更新
		show();  // 显示新画面
	}
	gameover();     // 游戏结束、后续处理
	return 0;
}
